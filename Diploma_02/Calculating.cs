﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Diploma_02
{
    public class Calculating
    {
        /// <summary>
        /// Оптимальные средние затраты
        /// </summary>
        public double G = 0.0;

        /// <summary>
        /// Количество итераций, которое понадобилось для расчета G
        /// </summary>
        /// <remarks>По сути является счетчиком бесконечного цикла. 
        /// Условиями выхода из этого цикла являются:
        /// <list type="bullet">
        /// <item>Достижение точности схождения ряда <code>|a(n) - a(n - 1)|<=epsilon</code></item> 
        /// <item>Достижение счетчика итераций максимально допосутимого значения Nmax</item>
        /// </list>
        /// <see cref="_epsilon"/>
        /// <see cref="_nmax"/>
        /// <see cref="Alpha_n"/>
        /// </remarks>
        public int N = 0;
        public List<List<double>> V { get; set; } = new List<List<double>>();

        /// <summary>
        /// Список стратегий управления
        /// </summary>
        public List<List<(int x1, int x2, bool ctrl)>> Fn { get; set; } = new List<List<(int x1, int x2, bool ctrl)>>();

        /// <summary>
        /// Список оптимальных средних затрат на каждой итерации расчета G
        /// </summary>
        /// <see cref="G"/>
        /// <remarks>G это Alpha_n, расчитанное на последней итерации.
        /// (По сути это последний элемент этого списка).</remarks>
        public List<double> Alpha_n { get; set; } = new List<double>();

        /// <summary>
        /// Матрица состояний
        /// </summary>
        /// <remarks>Хотя по сути это список состояний, ибо является стройкой в м элементов.</remarks>
        private List<(int x1, int x2)> S { get; set; } = new List<(int x1, int x2)>();

        /// <summary>
        /// Матрица вероятностей перехода из состояния i в состояние j при положении переключятеля "Выкл."
        /// </summary>
        private List<List<double>> P0 { get; set; } = new List<List<double>>();

        /// <summary>
        /// Матрица вероятностей перехода из состояния i в состояние j при положении переключятеля "Вкл."
        /// </summary>
        private List<List<double>> P1 { get; set; } = new List<List<double>>();

        /// <summary>
        /// Интенсивность входящего потока требований
        /// </summary>
        double _lambda;

        /// <summary>
        /// Интенсивность обслуживания требований во пераовй системе
        /// </summary>
        double _mu1;

        /// <summary>
        /// Интенсивность обслуживания требований во пераовй системе
        /// </summary>
        double _mu2;

        /// <summary>
        /// Стоимость обслуживания в первой системе
        /// </summary>
        double _c1;

        /// <summary>
        /// Стоимость обслуживания в второй системе
        /// </summary>
        double _c2;

        /// <summary>
        /// Точность
        /// </summary>
        /// <remarks>По умолчнию равна 1E-03</remarks>
        double _epsilon;

        /// <summary>
        /// Ограничение на число требований в каждой системе
        /// </summary>
        int _K;

        /// <summary>
        /// Максимальное допустимое число итераций
        /// </summary>
        int _nmax;

        /// <summary>
        ///  Просто переменная, в которую сохраняется _К+1
        /// </summary>
        private int k;

        /// <summary>
        /// Размерность матрицы состоний S
        /// </summary>
        /// <remarks>Рассчитывается как <code>(_K + 1)^2</code></remarks>
        /// <see cref="_K"/>
        private int m;

        /// <summary>
        /// Создает экземпляр класса Calculating и инициализирует его поля аргументами
        /// </summary>
        /// <param name="lambda">Интенсивность входящего потока требований</param>
        /// <param name="mu1">Интенсивность обслуживания требований во пераовй системе</param>
        /// <param name="mu2">Интенсивность обслуживания требований во пераовй системе</param>
        /// <param name="c1">Стоимость обслуживания в первой системе</param>
        /// <param name="c2">Стоимость обслуживания в второй системе</param>
        /// <param name="epsilon">Точность</param>
        /// <param name="K">Ограничение на число требований в каждой системе</param>
        /// <param name="nmax"><see cref="N"/></param>
        public Calculating(
            double lambda,
            double mu1,
            double mu2,
            double c1,
            double c2,
            double epsilon,
            int K,
            int nmax)
        {
            _lambda = lambda;
            _mu1 = mu1;
            _mu2 = mu2;
            _c1 = c1;
            _c2 = c2;
            _epsilon = epsilon;
            _K = K;
            _nmax = nmax;
        }

        public Calculating(VariableG variable)
        {
            _lambda = variable.Lambda;
            _mu1 = variable.Mu1;
            _mu2 = variable.Mu2;
            _c1 = variable.C1;
            _c2 = variable.C2;
            _epsilon = variable.Epsilon;
            _K = variable.K;
            _nmax = variable.Nmax;
        }

        /// <summary>
        /// Рассчитать оптимальные средние затраты G
        /// </summary>
        /// <remarks>Это главный метод, в котором производится основной рассчет
        /// показателей G и оптимальной стратегии управления F(n)</remarks>
        public void GetG()
        {
            GetS();
            GetP10();

            var c = GetC();

            Alpha_n = new List<double>();
            Alpha_n.Add(1E+12);
            Alpha_n.Add(1E+10);

            var n = 1;
            var v = new List<double>();

            for (int y = 0; y < m; y++)
                v.Add(0.0);

            V.Add(v);

            var fn = new List<(int x1, int x2, bool ctrl)>();

            for (int y = 0; y < m; y++)
                fn.Add((S[y].x1, S[y].x2, false));

            Fn.Add(fn);


            while (Math.Abs(Alpha_n[n] - Alpha_n[n - 1]) >= _epsilon)
            {
                fn = new List<(int x1, int x2, bool ctrl)>();
                v = new List<double>();

                for (int i = 0; i < m; i++)
                {
                    var sumP0v = 0.0;
                    var sumP1v = 0.0;

                    for (int r = 0; r < m; r++)
                    {
                        sumP0v += P0[i][r] * V[n - 1][r];  // 
                        sumP1v += P1[i][r] * V[n - 1][r];
                    }

                    var v0 = c[i] + sumP0v;
                    var v1 = c[i] + sumP1v;

                    if (v0 <= v1)
                    {
                        v.Add(v0);
                        fn.Add((S[i].x1, S[i].x2, false));
                    }
                    else
                    {
                        v.Add(v1);
                        fn.Add((S[i].x1, S[i].x2, true));
                    }
                }

                V.Add(v);
                Fn.Add(fn);

                var Amax = V[n][0] - V[n - 1][0];
                for (int i = 1; i < m; i++)
                {
                    var a = V[n][i] - V[n - 1][i];
                    if (a > Amax)
                    {
                        Amax = a;
                    }
                }

                Alpha_n.Add(Amax);


                if (n >= _nmax)
                    throw new IndexOutOfRangeException("\nДостигнуто ограничение на число итераций!");

                n++;
            }

            N = n;
            G = Alpha_n.LastOrDefault();
        }

        /// <summary>
        /// Рассчитать оптимальные средние затраты G в зависимости от интенсивности входящего потока требований
        /// </summary>
        /// <see cref="GetG"/>
        /// <param name="anyLambda">Интенсивность входящего потока требований</param>
        /// <remarks>Лямбда является переменной величиной, котороую можно задавать извне
        /// и строить график зависимости G от нее.</remarks>
        /// <returns>Оптимальные средние затраты</returns>
        public double GetGfromLambda(double anyLambda)
        {
            GetS();
            GetP10_lambda(anyLambda);

            var c = GetC();

            Alpha_n = new List<double>();
            Alpha_n.Add(1E+12);
            Alpha_n.Add(1E+10);

            var n = 1;
            var v = new List<double>();

            V = new List<List<double>>();

            for (int y = 0; y < m; y++)
                v.Add(0.0);

            V.Add(v);

            var fn = new List<(int x1, int x2, bool ctrl)>();

            for (int y = 0; y < m; y++)
                fn.Add((S[y].x1, S[y].x2, false));

            Fn.Add(fn);


            while (n <= _nmax)
            {
                fn = new List<(int x1, int x2, bool ctrl)>();
                v = new List<double>();

                for (int i = 0; i < m; i++)
                {
                    var sumP0v = 0.0;
                    var sumP1v = 0.0;

                    for (int r = 0; r < m; r++)
                    {
                        sumP0v += P0[i][r] * V[n - 1][r];  // 
                        sumP1v += P1[i][r] * V[n - 1][r];
                    }

                    var v0 = c[i] + sumP0v;
                    var v1 = c[i] + sumP1v;

                    if (v0 <= v1)
                    {
                        v.Add(v0);
                        fn.Add((S[i].x1, S[i].x2, false));
                    }
                    else
                    {
                        v.Add(v1);
                        fn.Add((S[i].x1, S[i].x2, true));
                    }
                }

                V.Add(v);
                Fn.Add(fn);

                var Amax = V[n][0] - V[n - 1][0];
                for (int i = 0; i < m; i++)
                {
                    var a = V[n][i] - V[n - 1][i];
                    if (a > Amax)
                    {
                        Amax = a;
                    }
                }

                Alpha_n.Add(Amax);

                if (Math.Abs(Alpha_n[n] - Alpha_n[n - 1]) < _epsilon)
                    break;

                n++;
            }

            N = n;
            G = Alpha_n.LastOrDefault();

            return G;
        }

        /// <summary>
        /// Рассчитать оптимальные средние затраты G в зависимости от интенсивности обслуживания требований во второй системе
        /// </summary>
        /// <see cref="GetG"/>
        /// <param name="anyMu2">Интенсивность обслуживания требований во второй системе</param>
        /// <returns>Оптимальные средние затраты</returns>
        public double GetGfromMu2(double anyMu2)
        {
            GetS();
            GetP10_mu2(anyMu2);

            var c = GetC();

            Alpha_n = new List<double>();
            Alpha_n.Add(1E+12);
            Alpha_n.Add(1E+10);

            V = new List<List<double>>();

            var n = 1;
            var v = new List<double>();

            for (int y = 0; y < m; y++)
                v.Add(0.0);

            V.Add(v);

            var fn = new List<(int x1, int x2, bool ctrl)>();

            for (int y = 0; y < m; y++)
                fn.Add((S[y].x1, S[y].x2, false));

            Fn.Add(fn);


            while (n <= _nmax)
            {
                fn = new List<(int x1, int x2, bool ctrl)>();
                v = new List<double>();

                for (int i = 0; i < m; i++)
                {
                    var sumP0v = 0.0;
                    var sumP1v = 0.0;

                    for (int r = 0; r < m; r++)
                    {
                        sumP0v += P0[i][r] * V[n - 1][r];  // 
                        sumP1v += P1[i][r] * V[n - 1][r];
                    }

                    var v0 = c[i] + sumP0v;
                    var v1 = c[i] + sumP1v;

                    if (v0 <= v1)
                    {
                        v.Add(v0);
                        fn.Add((S[i].x1, S[i].x2, false));
                    }
                    else
                    {
                        v.Add(v1);
                        fn.Add((S[i].x1, S[i].x2, true));
                    }
                }

                V.Add(v);
                Fn.Add(fn);

                var Amax = V[n][0] - V[n - 1][0];
                for (int i = 1; i < m; i++)
                {
                    var a = V[n][i] - V[n - 1][i];
                    if (a > Amax)
                    {
                        Amax = a;
                    }
                }

                Alpha_n.Add(Amax);

                if (Math.Abs(Alpha_n[n] - Alpha_n[n - 1]) < _epsilon)
                    break;

                n++;
            }

            N = n;
            G = Alpha_n.LastOrDefault();

            return G;
        }

        /// <summary>
        /// Рассчитать оптимальные средние затраты G в зависимости от стоимости обслуживания в первой системе
        /// </summary>
        /// <see cref="GetG"/>
        /// <param name="anyC1">Стоимость обслуживания в первой системе</param>
        /// <returns>Оптимальные средние затраты</returns>
        public double GetGFromC1(double anyC1)
        {
            GetS();
            GetP10();

            var c = GetC(anyC1);

            Alpha_n = new List<double>();
            Alpha_n.Add(1E+12);
            Alpha_n.Add(1E+10);

            V = new List<List<double>>();

            var n = 1;
            var v = new List<double>();

            for (int y = 0; y < m; y++)
                v.Add(0.0);

            V.Add(v);

            var fn = new List<(int x1, int x2, bool ctrl)>();

            for (int y = 0; y < m; y++)
                fn.Add((S[y].x1, S[y].x2, false));

            Fn.Add(fn);

            while (n <= _nmax)
            {
                fn = new List<(int x1, int x2, bool ctrl)>();
                v = new List<double>();

                for (int i = 0; i < m; i++)
                {
                    var sumP0v = 0.0;
                    var sumP1v = 0.0;

                    for (int r = 0; r < m; r++)
                    {
                        sumP0v += P0[i][r] * V[n - 1][r];  // 
                        sumP1v += P1[i][r] * V[n - 1][r];
                    }

                    var v0 = c[i] + sumP0v;
                    var v1 = c[i] + sumP1v;

                    if (v0 <= v1)
                    {
                        v.Add(v0);
                        fn.Add((S[i].x1, S[i].x2, false));
                    }
                    else
                    {
                        v.Add(v1);
                        fn.Add((S[i].x1, S[i].x2, true));
                    }
                }

                V.Add(v);
                Fn.Add(fn);

                var Amax = V[n][0] - V[n - 1][0];
                for (int i = 1; i < m; i++)
                {
                    var a = V[n][i] - V[n - 1][i];
                    if (a > Amax)
                    {
                        Amax = a;
                    }
                }

                Alpha_n.Add(Amax);

                if (Math.Abs(Alpha_n[n] - Alpha_n[n - 1]) < _epsilon)
                    break;

                n++;
            }

            N = n;
            G = Alpha_n.LastOrDefault();

            return G;
        }

        /// <summary>
        /// Генерация множества состояний сети
        /// </summary>
        /// <remarks>Заполняет поле S, которое является матрицей М х М</remarks>
        private void GetS()
        {
            k = _K + 1;
            m = (int)Math.Pow(k, 2);

            for (int i = 0; i < k; i++)
            {
                for (int j = 0; j < k; j++)
                {
                    S.Add((i, j));
                }
            }
        }

        private void ZeroP0()
        {
            P0 = new List<List<double>>();

            for (int i = 0; i < m; i++)
            {
                var p = new List<double>();
                for (int j = 0; j < m; j++)
                {
                    p.Add(0.0);
                }
                P0.Add(p);
            }
        }

        private void ZeroP1()
        {
            P1 = new List<List<double>>();

            for (int i = 0; i < m; i++)
            {
                var p = new List<double>();
                for (int j = 0; j < m; j++)
                {
                    p.Add(0.0);
                }
                P1.Add(p);
            }
        }

        /// <summary>
        /// Получить конкретное значение Непосредственных ожидаемых затрат
        /// </summary>
        /// <param name="x1">Состояние первой системы</param>
        /// <param name="x2">Состояние второй системы</param>
        /// <returns>Непосредственные ожидаемые затраты</returns>
        private double GetC(int x1, int x2)
        {
            return (_c1 * x1) + (_c2 * x2);
        }

        /// <summary>
        /// Получить конкретное значение Непосредственных ожидаемых затрат в зависимости от С1
        /// </summary>
        /// <remarks>C1 - переменная величина</remarks>
        /// <param name="x1">Состояние первой системы</param>
        /// <param name="x2">Состояние второй системы</param>
        /// <param name="anyC1">Стоимость ожиданий требований в первой системе</param>
        /// <returns>Непосредственные ожидаемые затраты</returns>
        private double GetC(int x1, int x2, double anyC1)
        {
            return (anyC1 * x1) + (_c2 * x2);
        }

        /// <summary>
        /// Получить заполненный массив Непосредственных ожидаемых затрат
        /// </summary>
        /// <remarks>Массив С заполняется на основе поля S (матрица состояний системы)</remarks>
        /// <returns>Заполненный массив Непосредственных ожидаемых затрат</returns>
        private List<double> GetC()
        {
            var c = new List<double>();

            for (int t = 0; t < m; t++)
            {
                var c_ = GetC(S[t].x1, S[t].x2);
                c.Add(c_);
            }

            return c;
        }

        /// <summary>
        /// Получить заполненный массив Непосредственных ожидаемых затрат в зависимости от внещнего параметра С1
        /// </summary>
        /// <param name="anyC1">Стоимость ожиданий требований в первой системе</param>
        /// <remarks>Является внешним переменным аргументов</remarks>
        /// <returns>Заполненный массив Непосредственных ожидаемых затрат</returns>
        private List<double> GetC(double anyC1)
        {
            var c = new List<double>();

            for (int t = 0; t < m; t++)
            {
                var c_ = GetC(S[t].x1, S[t].x2, anyC1);
                c.Add(c_);
            }

            return c;
        }

        /// <summary>
        /// Заполнение матриц вероятностей перехода систем в произвольные состояния
        /// </summary>
        /// <remarks>Матрицы Р имеют размерность М х М и строятся на основе матрицы состояний S</remarks>
        private void GetP10()
        {
            ZeroP0();
            ZeroP1();

            var p0Summ = 0.0;
            var p1Summ = 0.0;

            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    if (S[i].x2 == S[j].x2 && S[j].x1 - S[i].x1 == 1)
                        P0[i][j] = _lambda / (_lambda + _mu1 + _mu2);
                    else
                    if (S[i].x1 == S[j].x1 && S[i].x2 - S[j].x2 == 1)
                        P0[i][j] = _mu2 / (_lambda + _mu1 + _mu2);
                    else
                    if (S[i].x1 == S[j].x1 && S[i].x2 == S[j].x2)
                        P0[i][j] = _mu1 / (_lambda + _mu1 + _mu2);
                    else
                    if (S[i].x1 == S[j].x1 && S[i].x2 == 0 && S[j].x2 == 0)
                        P0[i][j] = (_mu1 + _mu2) / (_lambda + _mu1 + _mu2);

                    p0Summ += P0[i][j];

                    if (S[i].x2 == S[j].x2 && S[j].x1 - S[i].x1 == 1)
                        P1[i][j] = _lambda / (_lambda + _mu1 + _mu2);
                    else
                    if ((S[i].x1 - S[j].x1 == 1) && (S[j].x2 - S[i].x2 == 1))
                        P1[i][j] = _mu1 / (_lambda + _mu1 + _mu2);
                    else
                    if ((S[i].x1) == S[j].x1 && (S[i].x2 - S[j].x2 == 1))
                        P1[i][j] = _mu2 / (_lambda + _mu1 + _mu2);
                    else
                    if (S[i].x2 == S[j].x2 && S[i].x1 == 0 && S[j].x1 == 0)
                        P1[i][j] = _mu1 / (_lambda + _mu1 + _mu2);
                    else
                    if (S[i].x1 == S[j].x1 && S[i].x2 == 0 && S[j].x2 == 0)
                        P1[i][j] = _mu2 / (_lambda + _mu1 + _mu2);
                    else
                    if (S[i].x1 == 0 && S[i].x2 == 0 && S[j].x1 == 0 && S[j].x2 == 0)
                        P1[i][j] = (_mu1 + _mu2) / (_lambda + _mu1 + _mu2);

                    p1Summ += P1[i][j];
                }

                if (p0Summ != 1)
                {
                    P0[i][i] += 1 - p0Summ;
                }

                if (p1Summ != 1)
                {
                    P1[i][i] += 1 - p1Summ;
                }

                p0Summ = 0.0;
                p1Summ = 0.0;
            }
        }

        /// <summary>
        /// Заполнение матриц вероятностей перехода систем в произвольные состояния
        /// </summary>
        /// <param name="anyLambda">Интенсивность входящего потока требований</param>
        private void GetP10_lambda(double anyLambda)
        {
            ZeroP0();
            ZeroP1();

            var p0Summ = 0.0;
            var p1Summ = 0.0;

            var devider = (anyLambda + _mu1 + _mu2);
            devider = devider == 0 ? 1 : devider;

            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    if (S[i].x2 == S[j].x2 && S[j].x1 - S[i].x1 == 1)
                        P0[i][j] = anyLambda / devider;
                    else
                    if (S[i].x1 == S[j].x1 && S[i].x2 - S[j].x2 == 1)
                        P0[i][j] = _mu2 / devider;
                    else
                    if (S[i].x1 == S[j].x1 && S[i].x2 == S[j].x2)
                        P0[i][j] = _mu1 / devider;
                    else
                    if (S[i].x1 == S[j].x1 && S[i].x2 == 0 && S[j].x2 == 0)
                        P0[i][j] = (_mu1 + _mu2) / devider;

                    p0Summ += P0[i][j];

                    if (S[i].x2 == S[j].x2 && S[j].x1 - S[i].x1 == 1)
                        P1[i][j] = anyLambda / devider;
                    else
                    if ((S[i].x1 - S[j].x1 == 1) && (S[j].x2 - S[i].x2 == 1))
                        P1[i][j] = _mu1 / devider;
                    else
                    if ((S[i].x1) == S[j].x1 && (S[i].x2 - S[j].x2 == 1))
                        P1[i][j] = _mu2 / devider;
                    else
                    if (S[i].x2 == S[j].x2 && S[i].x1 == 0 && S[j].x1 == 0)
                        P1[i][j] = _mu1 / devider;
                    else
                    if (S[i].x1 == S[j].x1 && S[i].x2 == 0 && S[j].x2 == 0)
                        P1[i][j] = _mu2 / devider;
                    else
                    if (S[i].x1 == 0 && S[i].x2 == 0 && S[j].x1 == 0 && S[j].x2 == 0)
                        P1[i][j] = (_mu1 + _mu2) / devider;

                    p1Summ += P1[i][j];
                }

                if (p0Summ != 1)
                {
                    P0[i][i] += 1 - p0Summ;
                }

                if (p1Summ != 1)
                {
                    P1[i][i] += 1 - p1Summ;
                }

                p0Summ = 0.0;
                p1Summ = 0.0;
            }
        }

        /// <summary>
        /// Заполнение матриц вероятностей перехода систем в произвольные состояния
        /// </summary>
        /// <param name="anyMu2">Интенсивность обслуживания требований во второй системе</param>
        private void GetP10_mu2(double anyMu2)
        {
            ZeroP0();
            ZeroP1();

            var p0Summ = 0.0;
            var p1Summ = 0.0;

            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    if (S[i].x2 == S[j].x2 && S[j].x1 - S[i].x1 == 1)
                        P0[i][j] = _lambda / (_lambda + _mu1 + anyMu2);
                    else
                    if (S[i].x1 == S[j].x1 && S[i].x2 - S[j].x2 == 1)
                        P0[i][j] = anyMu2 / (_lambda + _mu1 + anyMu2);
                    else
                    if (S[i].x1 == S[j].x1 && S[i].x2 == S[j].x2)
                        P0[i][j] = _mu1 / (_lambda + _mu1 + anyMu2);
                    else
                    if (S[i].x1 == S[j].x1 && S[i].x2 == 0 && S[j].x2 == 0)
                        P0[i][j] = (_mu1 + anyMu2) / (_lambda + _mu1 + anyMu2);

                    p0Summ += P0[i][j];

                    if (S[i].x2 == S[j].x2 && S[j].x1 - S[i].x1 == 1)
                        P1[i][j] = _lambda / (_lambda + _mu1 + anyMu2);
                    else
                    if ((S[i].x1 - S[j].x1 == 1) && (S[j].x2 - S[i].x2 == 1))
                        P1[i][j] = _mu1 / (_lambda + _mu1 + anyMu2);
                    else
                    if ((S[i].x1) == S[j].x1 && (S[i].x2 - S[j].x2 == 1))
                        P1[i][j] = anyMu2 / (_lambda + _mu1 + anyMu2);
                    else
                    if (S[i].x2 == S[j].x2 && S[i].x1 == 0 && S[j].x1 == 0)
                        P1[i][j] = _mu1 / (_lambda + _mu1 + anyMu2);
                    else
                    if (S[i].x1 == S[j].x1 && S[i].x2 == 0 && S[j].x2 == 0)
                        P1[i][j] = anyMu2 / (_lambda + _mu1 + anyMu2);
                    else
                    if (S[i].x1 == 0 && S[i].x2 == 0 && S[j].x1 == 0 && S[j].x2 == 0)
                        P1[i][j] = (_mu1 + anyMu2) / (_lambda + _mu1 + anyMu2);

                    p1Summ += P1[i][j];
                }

                if (p0Summ != 1)
                {
                    P0[i][i] += 1 - p0Summ;
                }

                if (p1Summ != 1)
                {
                    P1[i][i] += 1 - p1Summ;
                }

                p0Summ = 0.0;
                p1Summ = 0.0;
            }
        }
    }
}
