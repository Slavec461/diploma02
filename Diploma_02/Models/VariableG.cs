﻿namespace Diploma_02
{
    public class VariableG
    {
        public double Lambda { get; set; }
        public double Mu1 { get; set; }
        public double Mu2 { get; set; }
        public double C1 { get; set; }
        public double C2 { get; set; }
        public double Epsilon { get; set; }
        public int K { get; set; }
        public int Nmax { get; set; }
    }
}
