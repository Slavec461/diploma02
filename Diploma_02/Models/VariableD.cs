﻿namespace Diploma_02
{
    public class VariableD
    {
        public double LambdaFrom { get; set; }
        public double LambdaTo { get; set; }
        public double LambdaStep { get; set; }

        public double Mu2From { get; set; }
        public double Mu2To { get; set; }
        public double Mu2Step { get; set; }

        public double C1From { get; set; }
        public double C1To { get; set; }
        public double C1Step { get; set; }
    }
}
