﻿using System;
using System.Collections.Generic;

namespace Diploma_02
{
    public class GraphicDataGetter
    {
        public Calculating _calcG { get; private set; }
        private VariableD _variableD;

        public GraphicDataGetter(VariableD variableD)
        {
            _variableD = variableD;
        }

        public List<(double Lambda, double G)> GetGFromLambda(VariableG gSettings)
        {
            _calcG = new Calculating(gSettings);
            List<(double Lambda, double G)> GFromLambda = new List<(double Lambda, double G)>();

            var currentLambda = _variableD.LambdaFrom;

            while (Math.Round(currentLambda, 1) <= Math.Round(_variableD.LambdaTo, 1))
            {
                var g = _calcG.GetGfromLambda(currentLambda);

                GFromLambda.Add((Math.Round(currentLambda, 1), Math.Round(g, 15)));

                currentLambda += _variableD.LambdaStep;
            }

            return GFromLambda;
        }

        public List<(double Mu2, double G)> GetGFromMu2(VariableG gSettings)
        {
            _calcG = new Calculating(gSettings);
            List<(double Mu2, double G)> GFromMu2 = new List<(double Mu2, double G)>();

            var currentMu2 = _variableD.Mu2From;

            while (Math.Round(currentMu2, 1) <= Math.Round(_variableD.Mu2To, 1))
            {
                var g = _calcG.GetGfromMu2(currentMu2);
                GFromMu2.Add((Math.Round(currentMu2, 1), Math.Round(g, 15)));
                currentMu2 += _variableD.Mu2Step;
            }

            return GFromMu2;
        }

        public List<(double C1, double G)> GetGFromC1(VariableG gSettings)
        {
            _calcG = new Calculating(gSettings);
            List<(double C1, double G)> GFromC1 = new List<(double C1, double G)>();

            var currentC1 = _variableD.C1From;

            while (Math.Round(currentC1, 1) < Math.Round(_variableD.C1To, 1))
            {
                var g = _calcG.GetGFromC1(currentC1);
                GFromC1.Add((Math.Round(currentC1, 1), Math.Round(g, 15)));
                currentC1 += _variableD.C1Step;
            }

            return GFromC1;
        }
    }
}
