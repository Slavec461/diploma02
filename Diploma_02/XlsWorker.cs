﻿using System;
using System.Collections.Generic;
using SpreadsheetGear.Charts;

namespace Diploma_02
{
    public class XlsWorker
    {
        /// <summary>
        /// Создать *.xls файл, содержащий диаграмму
        /// </summary>
        /// <param name="inputData"></param>
        /// <param name="arg">Это значение от которого рассчитывается функция</param>
        /// <remarks>
        /// <Example>например lambda или Mu2</Example>
        /// </remarks>
        /// <param name="value">Значение функции</param>
        /// <param name="round">Количество знаков для округления. По умолчанию 4</param>
        /// <remarks>G(Mu2) - вот это самое G</remarks>
        /// <returns>1 - успех, 0 - нет</returns>
        public static void CreateDiagram(List<(double Arg, double Val)> inputData, string arg, string value, int round = 4)
        {
            // Create a new workbook.
            SpreadsheetGear.IWorkbook workbook = SpreadsheetGear.Factory.GetWorkbook();
            SpreadsheetGear.IWorksheet worksheet = workbook.Worksheets["Sheet1"];//$"{value}({arg})"
            worksheet.Name = $"{value}({arg})";
            SpreadsheetGear.IWorksheetWindowInfo windowInfo = worksheet.WindowInfo;
            SpreadsheetGear.IRange cells = worksheet.Cells;

            // Load some sample data.
            cells[0, 0].Value = arg;
            cells[1, 0].Value = value;

            for (int i = 0; i < inputData.Count; i++)
            {
                cells[0, i + 1].Value = inputData[i].Arg;
                cells[1, i + 1].Value = Math.Round(inputData[i].Val, round);
            }

            // Add a chart to the worksheet's shape collection.
            // NOTE: Calculate the coordinates of the chart by converting row and column
            //       coordinates to points.  Use fractional row and colum values to get 
            //       coordinates anywhere in between row and column boundaries.
            double left = 10;
            double top = windowInfo.RowToPoints(2.5);
            double right = windowInfo.ColumnToPoints(16.0) - 10;
            double bottom = windowInfo.RowToPoints(24.5);

            SpreadsheetGear.Charts.IChart chart =
                worksheet.Shapes.AddChart(left, top, right - left, bottom - top).Chart;

            // Set the chart's source data range, plotting series in columns.
            SpreadsheetGear.IRange source = cells[1, 1, 1, inputData.Count];//$"A2:B{inputData.Count}"
            chart.SetSourceData(source, SpreadsheetGear.Charts.RowCol.Rows);

            // Set the chart type.
            chart.ChartType = SpreadsheetGear.Charts.ChartType.LineMarkersStacked;

            // Get a reference to the chart's series collection and each series.
            SpreadsheetGear.Charts.ISeriesCollection seriesCollection = chart.SeriesCollection;
            SpreadsheetGear.Charts.ISeries MainGraph = seriesCollection[0];

            // Add series name and argument steps from data
            MainGraph.Name = $"{value}({arg})";
            MainGraph.XValues = cells[0, 1, 0, inputData.Count];
           
            // Change the last series chart type and plot it on the secondary axis. 
            // NOTE: This creates a combination chart using multiple chart groups
            //       and utilizes both pmrimary and secondary axes sets, allowing for
            //       disproportionate ranges of values to be plotted separately.           

            // Change the fill color of each primary series.
            MainGraph.Format.Fill.ForeColor.RGB = SpreadsheetGear.Colors.DarkOrange;

            // Add data labels to the total series and get a reference to the data labels.
            MainGraph.HasDataLabels = true;
            SpreadsheetGear.Charts.IDataLabels dataLabels = MainGraph.DataLabels;

            // Position each data label below each data point.
            dataLabels.Position = SpreadsheetGear.Charts.DataLabelPosition.Below;

            // Change the fill formatting of the data labels.
            SpreadsheetGear.Shapes.IFillFormat fillFormat = dataLabels.Format.Fill;
            fillFormat.ForeColor.RGB = SpreadsheetGear.SystemColors.Window;
            fillFormat.Visible = true;

            // Change the line formatting of the data labels.
            SpreadsheetGear.Shapes.ILineFormat lineFormat = dataLabels.Format.Line;
            lineFormat.ForeColor.RGB = SpreadsheetGear.SystemColors.WindowText;
            lineFormat.Visible = true;

            // Change the legend position to the top of the chart.
            chart.Legend.Position = SpreadsheetGear.Charts.LegendPosition.Bottom;

            // Get a reference to the primary value axis, hide major
            // gridlines, and use a fixed major unit scaling value.
            SpreadsheetGear.Charts.IAxis valueAxis =
                chart.Axes[SpreadsheetGear.Charts.AxisType.Value];

            valueAxis.HasTitle = true;
            valueAxis.AxisTitle.Text = $"{value}";
            valueAxis.AxisTitle.Position = ChartElementPosition.Automatic;
            valueAxis.AxisTitle.Orientation = 0;

            valueAxis.HasMajorGridlines = true;
            valueAxis.MajorGridlines.Format.Line.Visible = false;
            valueAxis.MajorUnit = 0.1;

            valueAxis.Crosses = SpreadsheetGear.Charts.AxisCrosses.Custom;
            valueAxis.CrossesAt = 0.0;

            SpreadsheetGear.Charts.IAxis categoryAxis =
                chart.Axes[SpreadsheetGear.Charts.AxisType.Category];

            categoryAxis.HasTitle = true;
            categoryAxis.AxisTitle.Text = $"{arg}";

            categoryAxis.HasMajorGridlines = true;
            categoryAxis.MajorUnit = 1;
            categoryAxis.MajorGridlines.Format.Line.Visible = true;
            categoryAxis.MajorGridlines.Format.Line.Weight = 0.005;

            // Change the chart font name and size.
            chart.ChartArea.Font.Name = "Verdana";
            chart.ChartArea.Font.Size = 8;

            // Add a chart title and change the font size.
            chart.HasTitle = true;
            chart.ChartTitle.Text = "  ";//$"{value}({arg})";
            chart.ChartTitle.Font.Size = 9;

            workbook.SaveAs($"{value}({arg}).xls", SpreadsheetGear.FileFormat.Excel8);
        }
    }
}
