﻿using System.Collections.Generic;
using System.Text;

namespace Diploma_02
{
    public static class LatexFormater
    {
        public static string GetFormated(
            List<(int x1, int x2, bool ctrl)> inputData1,
            List<(int x1, int x2, bool ctrl)> inputData2,
            string litera,
            double val1,
            double val2)
        {
            var sb = new StringBuilder();

            // Format header
            sb.AppendLine(@"\begin{table}
\caption{\label{tab:bolts} Any caption}
\begin{center}
\begin{tabular}{|c|c|c|}
\hline
& \multicolumn{2}{c|}{f(i)} \\
\cline{2-3}
\raisebox{1.5ex}[0cm][0cm]{i}");
            sb.AppendLine($"& ${litera}$ = {val1} & ${litera}$ = {val2} \\\\ \\hline");


            // Fill table with colemns
            // replace repeated ctrls with "..."
            var farmatedData = ReplaceRepitedValues(inputData1, inputData2);
            sb.Append(farmatedData);

            // Close table
            sb.Append("\\hline\r\n");
            sb.Append("\\end{tabular}\r\n");
            sb.Append("\\end{center}\r\n");
            sb.Append("\\end{table}\r\n");

            return sb.ToString();
        }

        private static string ReplaceRepitedValues(
            List<(int x1, int x2, bool ctrl)> values1,
            List<(int x1, int x2, bool ctrl)> values2)
        {
            var aggregatedData = new List<(string si, string ctrl1, string ctrl2)>();
            for (int i = 0; i < values1.Count; i++)
            {
                aggregatedData.Add(
                    ($"({values1[i].x1},{values1[i].x2})",
                         values1[i].ctrl ? "Вкл." : "Выкл.",
                         values2[i].ctrl ? "Вкл." : "Выкл."));
            }

            for (int i = 0; i < aggregatedData.Count; i++)
            {
                var alreadyDel = false;

                for (int j = i + 1; j < aggregatedData.Count - 1; j++)
                {
                    var tmpF = (aggregatedData[j].si, aggregatedData[j].ctrl1, aggregatedData[j].ctrl2);

                    if (aggregatedData[i].ctrl1 == aggregatedData[j].ctrl1 &&
                        aggregatedData[i].ctrl2 == aggregatedData[j].ctrl2)
                    {
                        if (!alreadyDel)
                        {                            
                            aggregatedData[j] = ("...", "...", "...");
                            alreadyDel = true;                            
                        }
                        else
                        {
                            aggregatedData.Remove(aggregatedData[j]);
                        }
                    }
                    else
                    {
                        if (alreadyDel)
                        {
                            //aggregatedData.Insert(j-1, tmpF);
                            i = j+1;
                            break;
                        }
                        else
                        {                           
                            break;
                        }
                    }
                }
            }

            // Формируем текст результата
            var sb = new StringBuilder();
            foreach (var fn in aggregatedData)
            {
                sb.AppendLine($"{fn.si} & {fn.ctrl1} & {fn.ctrl2} \\\\");
            }

            return sb.ToString();
        }
    }
}
