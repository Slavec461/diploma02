﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace Diploma_02
{
    class Program
    {
        static void Main(string[] args)
        {
            const int round = 3;

            //var f1 = new List<(int, int, bool)>() { (1, 2, false), (1, 3, true), (1, 4, true), (1, 5, true), (1, 6, true), (1, 7, false), (1, 2, false), (1, 3, true), (1, 4, true), (1, 5, true), (1, 6, true), (1, 7, false) };
            //var f2 = new List<(int, int, bool)>() { (2, 2, false), (2, 3, true), (1, 4, true), (2, 5, true), (2, 6, true), (2, 7, false), (1, 2, false), (1, 3, true), (1, 4, true), (1, 5, true), (1, 6, true), (1, 7, false) };

            //var header = LatexFormater.GetFormated(f1, f2, "\\mu", 11, 22);
            //Console.WriteLine(header);
            //return;

            Console.WriteLine("Файл входных данных \"input.txt\" должен иметь следующий формат:");
            Console.WriteLine(" lambda\n mu1\n mu2\n c1\n c2\n e\n K\n nmax\n");
            Console.WriteLine("K - ограничение на число требований в каждой системе");
            Console.WriteLine("lambda -интенсивность поступления требований в первую систему");
            Console.WriteLine("mu1 - интенсивность обслуживания требований в первой системе");
            Console.WriteLine("mu2 - интенсивность обслуживания требований во второй системе");
            Console.WriteLine("c1 - стоимость ожидания для требований в первой системе");
            Console.WriteLine("c2 - стоимость ожидания для требований во второй системе");
            Console.WriteLine("nmax - максимальное число шагов алгоритма");
            Console.WriteLine("epsilon - малое значение точности вычислений");

            //var savedVars = new Variable()
            //{
            //    //Lambda = 3.0,  
            //    Lambda = 3.0,
            //    //Mu1 = 8.0,
            //    Mu1 = 4.0,
            //    //Mu2 = 6.0,
            //    Mu2 = 6.0,
            //    //C1 = 1.0,
            //    C1 = 2.0,
            //    //C2 = 3.0,
            //    C2 = 4.0,
            //    K = 2,
            //    //K = 5,
            //    Epsilon = 0.001,
            //    Nmax = 500
            //};
            //var json = JsonConvert.SerializeObject(savedVars);
            //File.WriteAllText("input.txt", json);

            var varsFromFile = File.ReadAllText("input.txt");
            try
            {
                var vars = JsonConvert.DeserializeObject<VariableG>(varsFromFile);

                var calc = new Calculating(lambda: vars.Lambda,
                                           mu1: vars.Mu1,
                                           mu2: vars.Mu2,
                                           c1: vars.C1,
                                           c2: vars.C2,
                                           epsilon: vars.Epsilon,
                                           K: vars.K,
                                           nmax: vars.Nmax);
                calc.GetG();

                var m = Math.Pow(vars.K + 1, 2);
                var sb = new StringBuilder();

                //// Заголовок таблица
                //sb.Append("n");

                //for (int i = 0; i < m; i++)
                //{
                //    sb.Append($"\tVn({i})");
                //}

                //sb.Append("\tAlpha_n");

                //for (int i = 0; i < m; i++)
                //{
                //    sb.Append($"\tFn({i})");
                //}

                //sb.Append(Environment.NewLine);

                //// Заполняем таблицу
                //for (int n = 0; n < calc.V.Count; n++)
                //{
                //    sb.Append($"{Environment.NewLine}{n}");
                //    for (int i = 0; i < calc.V[n].Count; i++)
                //    {
                //        sb.Append($"\t{Math.Round(calc.V[n][i], round)}");
                //    }

                //    var specifier = $"E{round - 1}";
                //    var culture = CultureInfo.CreateSpecificCulture(CultureInfo.CurrentCulture.TextInfo.CultureName);
                //    sb.Append($"\t{calc.Alpha_n[n].ToString(specifier, culture)}");

                //    for (int i = 0; i < calc.Fn[n].Count; i++)
                //    {
                //        var state = calc.Fn[n][i].ctrl ? "Вкл." : "Выкл.";
                //        sb.Append($"\t({calc.Fn[n][i].x1},{calc.Fn[n][i].x2}):{state}");
                //    }
                //}

                sb.AppendLine($"{Environment.NewLine}{Environment.NewLine}Результаты исследования:");
                sb.AppendLine($"Число итераций (n): {calc.N}");
                sb.AppendLine($"Оптимальные средние затраты (g): {Math.Round(calc.G, round)}");
                sb.AppendLine($"Оптимальная стратегия управления (Fn): ");
                sb.AppendLine($"Состояние сети\tУправление");
                for (int n = 0; n < calc.Fn[calc.Fn.Count - 1].Count; n++)
                {
                    var state = calc.Fn.Last()[n].ctrl ? "Вкл." : "Выкл.";
                    sb.AppendLine($"\t({calc.Fn.Last()[n].x1},{calc.Fn.Last()[n].x2})\t|{state}");
                }

                Console.WriteLine(sb.ToString());
                File.WriteAllText("output.txt", sb.ToString());
                                
                //-----------------------------------------------------------------------------------
                //Console.WriteLine();
                //Console.WriteLine("Таблицы для построения графиков:\r\n");

                var sb_table = new StringBuilder();
                var varD = new VariableD()
                {
                    LambdaFrom = 1.0,
                    LambdaTo = 2.0,
                    LambdaStep = 0.1,

                    Mu2From = 8.0,
                    Mu2To = 10.0,
                    Mu2Step = 0.1,

                    C1From = 1.0,
                    C1To = 3.0,
                    C1Step = 0.1
                };

                var graphDataGetter = new GraphicDataGetter(varD);

                // Данные для G(Lambda)
                var varLambda = new VariableG()
                {
                    K = 10,
                    Mu1 = 6.0,
                    Mu2 = 8.0,
                    C1 = 1.0,
                    C2 = 3.0,
                    Lambda = 1.0,
                    Nmax = 500,
                    Epsilon = 1E-03
                };

                var gFromLa = graphDataGetter.GetGFromLambda(varLambda);
                XlsWorker.CreateDiagram(gFromLa, "lambda", "g");

                sb_table.AppendLine("lambda;\tg");
                foreach (var gla in gFromLa)
                {
                    sb_table.AppendLine($"{gla.Lambda};\t{gla.G}");
                }
                Console.WriteLine();

                // Данные для G(Mu2)
                var varMu2 = new VariableG()
                {
                    K = 10,
                    Mu1 = 6.0,
                    Mu2 = 10.0,
                    C1 = 1.0,
                    C2 = 3.0,
                    Lambda = 1.0,
                    Nmax = 500,
                    Epsilon = 1E-03
                };

                var gFromMu2 = graphDataGetter.GetGFromMu2(varMu2);
                XlsWorker.CreateDiagram(gFromMu2, "mu2", "g");
                //var header = LatexFormater.GetFormated(graphDataGetter._calcG.Fn.First(), graphDataGetter._calcG.Fn.Last(), "\\mu", 11, 22);
                //Console.WriteLine(header);

                sb_table.AppendLine("Mu2;\tc");

                foreach (var gla in gFromMu2)
                {
                    sb_table.AppendLine($"{gla.Mu2};\t{gla.G}");
                }
                Console.WriteLine();

                // Данные для G(C1)
                var varC1 = new VariableG()
                {
                    K = 10,
                    Mu1 = 6.0,
                    Mu2 = 10.0,
                    C1 = 1.0,
                    C2 = 3.0,
                    Lambda = 1.0,
                    Nmax = 500,
                    Epsilon = 1E-03
                };

                var gFromC1 = graphDataGetter.GetGFromC1(varC1);
                XlsWorker.CreateDiagram(gFromC1, "c1", "g");
                //header = LatexFormater.GetFormated(graphDataGetter._calcG.Fn.First(), graphDataGetter._calcG.Fn.Last(), "c", 11, 22);
                //Console.WriteLine(header);

                sb_table.AppendLine("c1;\tg");

                foreach (var gla in gFromC1)
                {
                    sb_table.AppendLine($"{gla.C1};\t{gla.G}");
                }

                Console.WriteLine();
                //Console.WriteLine(sb_table.ToString());
                File.WriteAllText("table.csv", sb_table.ToString());
                Console.WriteLine("Рассчет окончен. Для завершения нажмите любую клавишу.");
                // Console.ReadKey();
            }
            catch (IndexOutOfRangeException ex1)
            {
                Console.WriteLine(ex1.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Что-то пошло не так!\r\nException: {ex.Message}");
            }
        }
    }
}
